<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index');
Route::put('/product', 'ProductController@create');
Route::post('/product/{id}', 'ProductController@update');
Route::delete('/product/{id}', 'ProductController@delete');

Route::get('excel/download', 'ExcelController@download');
Route::post('excel', 'ExcelController@test');
