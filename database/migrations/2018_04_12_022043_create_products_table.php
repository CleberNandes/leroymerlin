<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //\DB::select('create database leroyMerlin');

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category');
            $table->integer('lm');
            $table->string('name');
            $table->boolean('free_shipping');
            $table->string('description');
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        //\DB::select('drop database leroyMerlin');
    }
}
