<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Laravel</title>
	
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	
	<link href="/css/app.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	
	<!-- Styles -->
	<style>
		html, body {
			background-color: #fff;
			color: #636b6f;
			
			font-weight: 100;
			height: 100vh;
			margin: 0;
		}
		
		.flex-center {
			display: flex;
			justify-content: center;
		}
		
		.content {
			text-align: center;
			margin-top: 20px;
		}
		
		.title img {
			font-size: 84px;
			position: absolute;
			left: 30px;
			width: 150px;
		}
		
		.formFile {
			display: inline-block;
			margin-bottom: 20px;
		}
		
		.formFile .btn-success {
			float: right;
			margin-left: 40px;
		}
		
		.formFile .custom-file {
			float: left;
			width: 300px;
			
		}
		
		.formFile .btn-warning {
			float: left;
		}
		
		.custom-file-control:lang(en)::before {
			display: none;
		}
		
		.custom-file-control:lang(en):empty::after {
			content: "Escolha um arquivo de produtos...";
		}
		
		.edit, .edit:hover {
			color: #ffc107;
		}
		
		.delete, .delete:hover {
			color: #dc3545;
		}
		
		.edit, .delete {
			opacity: 0.2;
			cursor: pointer;
		}
		
		.edit:hover, .delete:hover {
			opacity: 1;
		}
		
		#botaoRecebido {
			display: none;
		}
		
	</style>
</head>
<body>
	<div class="flex-center">
		
		<div class="content">
			<div class="title">
				<img src="img/img.png" alt="">
			</div>
			<div class="formFile">
				<label class="custom-file">
					<input type="file" id="file" multiple class="custom-file-input" onchange="selectFile(this)">
					<span class="custom-file-control"></span>
				</label>
				<button class="btn btn-warning" disabled onclick="sendFile()" title="Primeiro escolha o arquivo">Enviar</button>
				<a href="excel/download" class="btn btn-default download" title="Download do arquivo exemplo">
					<i class="material-icons">file_download</i>
				</a>
				<button type="button" class="btn btn-success updateProduct" data-toggle="modal" 
				data-target=".addProduct" onclick="clean()">
				Adicionar
			</button>
		</div>
		
		<div class="links">
			<table class="table text-center table-striped table-condensed table-bordered table-hover responsive-table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Category</th>
						<th>LM</th>
						<th>Name</th>
						<th>Free Shipping</th>
						<th>Description</th>
						<th>Price</th>
						<th>Alterar</th>
						<th>Deletar</th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
					<tr class="product">
						<td>{{$product->id}}</td>
						<td>{{$product->category}}</td>
						<td>{{$product->lm}}</td>
						<td>{{$product->name}}</td>
						<td valor="{{$product->free_shipping}}">
							@if($product->free_shipping)
							Sim
							@else
							Não
							@endif
						</td>
						<td>{{$product->description}}</td>
						<td>{{$product->price}}</td>
						<td>
							<a class="edit" onclick="update(this)"><i class="material-icons">mode_edit</i></a>
						</td>
						<td>
							<a class="delete" data-toggle="modal" data-target="#deletarProduto" 
							productid="{{$product->id}}" onclick="setFormAction(this)">
							<i class="material-icons" >delete</i>
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade addProduct">
	<div class="modal-dialog">
		<div class="modal-content" style="padding: 0 20px; ">
			<form action="{{action('ProductController@create')}}" method="post" class="formProduct" role="form">
				<input value="{{ csrf_token()}}" type="hidden" name="_token">
				<input type="hidden" name="_method" value="PUT">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Adicionar Produto</h4>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
				</div>
				
				<!-- Modal Body -->
				<div class="modal-body" style="margin: -10px 50px;">
					
					
					<div class="form-group">
						<label class="control-label" for="inputPassword3" style="float: left;">Category</label>
						<input class="form-control" name="category" type="number">
					</div>
					
					<div class="form-group">
						<label class="control-label">LM</label>
						<input class="form-control" name="lm" type="number">
					</div>
					
					<div class="form-group">
						<label class="control-label">Name</label>
						<input class="form-control" name="name">
					</div>
					
					<div class="form-group" >
						<label class="control-label">Free Shipping</label>
						<select class="form-control" name="free_shipping">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Description</label>
						<input class="form-control" name="description">
					</div>
					
					<div class="form-group">
						<label class="control-label">Price</label>
						<input  name="price" class="form-control" type="number" step="0.01">
					</div>
					
				</div>
				
				<!-- Modal Footer -->
				<div class="modal-footer" style="margin: 0 20px;">
					<button class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button class="btn btn-primary" type="submit"> Cadastrar</button>
				</div>
			</form>
		</div>
	</div>
</div>



<!-- Modal -->
<div class="modal fade" id="deletarProduto">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Deletar Produto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Tem certeza que quer excluir esse produto?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<form action="" method="post" id="formDelete">
					<input value="{{ csrf_token()}}" type="hidden" name="_token">
					<input type="hidden" name="_method" value="delete">
					<button type="submit" class="btn btn-danger">Deletar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<button id="botaoRecebido" data-toggle="modal" data-target="#sendFile"></button>
<!-- Modal -->
<div class="modal fade sendFile" id="sendFile">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Arquivo Recebido</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				O seu arquivo está sendo processado em background.<br>
				Quando estiver concluido você será notificado.<br>
				Enquanto isso pode usar o sistema normalmente.<br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<script src="/js/app.js"></script>
<script src="/js/jquery.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script>
	function update(element){
		
		var id = $(element).parent().parent().find('td:nth-child(1)').text();
		var category = $(element).parent().parent().find('td:nth-child(2)').text();
		var lm = $(element).parent().parent().find('td:nth-child(3)').text();
		var name = $(element).parent().parent().find('td:nth-child(4)').text();
		var free_shipping = $(element).parent().parent().find('td:nth-child(5)').attr('valor');
		var description = $(element).parent().parent().find('td:nth-child(6)').text();
		var price = $(element).parent().parent().find('td:nth-child(7)').text();
		$('.updateProduct').click();
		$('[name="category"]').val(category);
		$('[name="lm"]').val(lm);
		$('[name="name"]').val(name);
		$('[name="free_shipping"]').val(free_shipping);
		$('[name="description"]').val(description);
		$('[name="price"]').val(price);
		$('.formProduct').attr('action','/product/'+id);
		$('[name="_method"]').val('post');
	}
	
	function clean(){
		$('[name="category"]').val("");
		$('[name="lm"]').val("");
		$('[name="name"]').val("");
		$('[name="free_shipping"]').val("");
		$('[name="description"]').val("");
		$('[name="price"]').val("");
		$('.formProduct').attr('action','/product');
		$('[name="_method"]').val('put');
	}
	
	function selectFile(ele){
		var fileName = $(ele).val().split('/').pop().split('\\').pop();
		$('.custom-file-control').text(fileName);
		$('.btn-warning').attr('disabled',false);
		$('.btn-warning').attr('title',"");
	}
	
	function setFormAction(ele){
		$('#formDelete').attr('action','/product/'+$(ele).attr('productid'))
	}
	
	function sendFile(){
		crsfToken = document.getElementsByName("_token")[0].value;
		var formData = new FormData($('#upload_form')[0]);
		console.log($('#file')[0].files[0])
		console.log(formData)
		formData.append('file', $('#file')[0].files[0]);
		$('.custom-file-control').text("");
		$('#file').val("");
		$('.btn-warning').attr('disabled',true);
		$('.btn-warning').attr('title',"Primeiro escolha o arquivo");
		$('#botaoRecebido').click();
		
		$.ajax({
			url: 'excel',
			data: formData,
			type: "POST",
			processData: false,
			//ContentType: false,
			DataType: 'json',
			headers: {
				"X-CSRF-Token": crsfToken
			},
			success: function(event){
				console.log();
				
			},
			error: function(json){
				console.log();
			}
		});
	}
	
</script>


</body>
</html>
