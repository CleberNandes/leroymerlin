<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExcelController extends Controller
{
	public function test(Request $req){
		
		dd($_FILES['file']);
		dd($req['file']);
		//$fileExcel = IOFactory::load($_FILES['file']['tmp_name']);

		foreach ($fileExcel as $Line => $Obj) {
			//$Power = $this->savePowerObject($Obj, $Line);
			//$this->saveViewerObject($Obj, $Power, $Line);
			//$this->lines = $Line;
		}
		//$worksheet = IOFactory::load('./file/products_teste_webdev_leroy.xlsx');
		//$array = $worksheet->getActiveSheet()->toArray(true, true, true, true);
	}
	
	public function import(){
		
	}
	
	public function download()
	{
		$file = base_path("file/products_teste_webdev_leroy.xlsx");
		
		if (is_file($file)) {
			header('Content-Description: File Transfer');
			header('Content-Type: ' . mime_content_type($file));
			header('Content-Disposition: inline; filename=' . basename($file));
			header('Expires: 0');
			header('Cache-Control: private');
			header('Pragma: private');
			header('Content-Length: ' . filesize($file));
			
			readfile($file);
			exit();
		} else {
			throw new \Exception('File not found');
		}
	}

	public function open($path, $File = '')
    {
        $xls = IOFactory::load($path . $File);
        $xls->setActiveSheetIndex(0);
        
        return $this->readSheet($xls->getActiveSheet());
    }

    /**
     * read given sheet and return data as array
     *
     * @param \PHPExcel_Worksheet $sheet
     *
     * @return array
     */
    private function readSheet(/* \PHPExcel_Worksheet */ $sheet, $returnCalculatedValue = false)
    {
        $return = [];
        
        foreach ($sheet->getRowIterator() as $row) {
            ///** @var \PHPExcel_Cell $cell */
            foreach ($row->getCellIterator() as $key => $cell) {
                $val = $cell->getCalculatedValue();
                if (! is_null($val)) {
                    $return[$cell->getrow()][$cell->getColumn()] = $returnCalculatedValue ? $val : $cell;
                }
            }
        }
        
        return $return;
    }
}
