<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function create(Request $req){
        $product = new Product;
        $product->fill($req->all());
        $product->save();
        return redirect()->action('ProductController@index');
    }

    public function update(Request $req, $id){
        $product = Product::find($id);
        $product->fill($req->all());
        $product->save();
        return redirect()->back();
    }

    public function delete($id){
        Product::destroy($id);
        return redirect()->back();
    }

    public function index(){
        $products = Product::all();

        return view('products',compact('products'));
    }
}
